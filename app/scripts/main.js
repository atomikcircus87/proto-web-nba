$(document).ready(function()
{
    var navItems = $('.menu-level-2 li > a');
    var navListItems = $('.menu-level-2 li');
    var allWells = $('.menu-level-2-content');
    var allWellsExceptFirst = $('.menu-level-2-content:not(:first)');
    
    allWellsExceptFirst.hide();
    navItems.click(function(e)
    {
        e.preventDefault();
        navListItems.removeClass('active');
        $(this).closest('li').addClass('active');
        
        allWells.hide();
        var target = $(this).attr('data-target-id');
        $('#' + target).show();
    });
});